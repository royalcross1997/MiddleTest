## 20202708 전인철 웹앱 프로그래밍 중간고사 대체 과제물

print("갤러그 게임 시작")
print("적 비행기 발생")
game_result = [] ## 전체 명령어 입력 저장용 list
game_one = 0 ## 1번 클릭 횟수 저장
game_two = 0 ## 2번 클릭 횟수 저장
game_three = 0 ## 3번 클릭 횟수 저장 

game_dict = {}
game_dict['1'] = "발사"
game_dict['2'] = "오른쪽으로 이동"
game_dict['3'] = "왼쪽으로 이동"
game_dict['4'] = "플레이를 종료합니다."
## 위에 부분까지 dict 활용하여 각 해당하는 명령어 입력시 해당 문구 출력되도록 함.

print("1. 발사 2. 오른쪽이동 3. 왼쪽이동 4. 게임 종료")

while True :
    num = input("명령을 선택해주세요 : ")
    game_result.append(num) ##game_result list안에 해당 명령어가 입력되었다는 사실을 추가함.
    
    for letter in num:
        if letter == '1':
            game_one = game_one + 1
        if letter == '2':
            game_two = game_two + 1
        if letter == '3':
            game_three = game_three + 1
## 1,2,3번이 입력 될때마다 횟수가 위쪽에 있는 game_one, game_two, game_three 값이 업데이트 됨.

    print("*****************")
    print(game_dict[num])
    print("*****************\n")
## game_dict에 각 숫자별로 저장 되어있던 내용들을 불러들임.

    if num == '4':
        print("*****************")
        print(game_dict[num])
        print("*****************\n")
        break
## 4가 입력시 게임 loop에서 빠져나오도록 함.

print("\n")
print("\n")
print("\n")
print("---------------------------------------------------")
print("지금까지 오른쪽으로 ", game_one, "번 이동하였습니다.\n")
print("지금까지 왼쪽으로 ", game_two, "번 이동하였습니다.\n")
print("지금까지 ", game_three, "번 발사 하였습니다.")
print("---------------------------------------------------")
## 위쪽까지는 해당 명령어 입력 받은 횟수를 저장 한 값 불러옴.

print("지금까지 사용한 명령어는 총 ",game_result,"입니다.")
## 이 명령어는 단순히 지금까지 이용했던 명령어 명단을 game_result에 저장 한 뒤 단순히 불러오는 용도